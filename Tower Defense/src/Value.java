
public class Value //creates level
{
	public static int airAir = -1;
	// early numbers are planet terrain
	public static int mars1 = 0;
	public static int mars2 = 1;
	public static int mars3 = 2;
	
	// 3 start ints are snowgoons
	public static int snowman00  = 30;  // black
	public static int snowman01  = 31;  // blue
	public static int snowman02  = 32;  // red
	
	// 5 start ints are spacemen
	public static int basic      = 50;  // basic
	public static int western    = 51;  // western
	public static int blowdryer  = 52;  // blowdryer
	public static int toaster0   = 53;  // toaster (none trapped)
	public static int toaster1   = 54;  // toaster ( one trapped)	
	public static int cherry     = 55;  // cherry-bomber
}
